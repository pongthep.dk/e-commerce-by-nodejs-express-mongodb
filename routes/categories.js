var express = require("express");
var router = express.Router();
var { check, validationResult } = require("express-validator");
var mongodb = require("mongodb");
var db = require("monk")("localhost:27017/E-CommerceDB");

router.get("/add", (req, res, next) => {
  var categories = db.get("categories");
  categories.find({}).then(category => {
    res.render("addcategory", { categories: category });
  });
});

router.post(
  "/add",
  [
    check("name", "กรอกหมวดหมู่สินค้า")
      .not()
      .isEmpty()
  ],
  (req, res, next) => {
    var result = validationResult(req);
    var errors = result.errors;
    if (!result.isEmpty()) {
      res.render("addcategory", { errors: errors });
    } else {
      var categories = db.get("categories");
      categories
        .insert({
          name: req.body.name
        })
        .then(success => {
          res.location("/");
          res.redirect("/");
        })
        .catch(err => {
          res.send(err);
        });
    }
  }
);

module.exports = router;
